<?php

$op1 = $_POST['operator_1'];
$op2 = $_POST['operator_2'];
$operation = $_POST['operator'];

$result = 0;

if($operation === '+'){
    $result = $op1 + $op2;
}

if($operation === '-'){
    $result = $op1 - $op2;
}

if($operation === '/'){
    $result = $op1 / $op2;
}

if($operation === '*'){
    $result = $op1 * $op2;
}


header("Content-Type: application/json");

$json = json_encode(array('result' => $result));

if ($json === false) {
    // Avoid echo of empty string (which is invalid JSON), and
    // JSONify the error message instead:
    $json = json_encode(array("jsonError", json_last_error_msg()));
    if ($json === false) {
        // This should not happen, but we go all the way now:
        $json = '{"jsonError": "unknown"}';
    }
    // Set HTTP response status code to: 500 - Internal Server Error
    http_response_code(500);
}
echo $json;
?>